
package com.wevat.rest.security;

import com.wevat.auth.service.WevatAuthenticationService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.security.core.Authentication;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;


public class JWTAuthenticationFilter extends GenericFilterBean {

    private final WevatAuthenticationService authenticationService;

    public JWTAuthenticationFilter(final WevatAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    public void doFilter(ServletRequest request,
            ServletResponse response,
            FilterChain filterChain)
            throws IOException, ServletException {
        Authentication authentication = authenticationService
                .getAuthentication((HttpServletRequest) request, (HttpServletResponse) response);
        
        SecurityContextHolder.getContext()
                    .setAuthentication(authentication);
        

        filterChain.doFilter(request, response);

    }
}
