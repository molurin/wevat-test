package com.wevat.rest;

import com.wevat.auth.data.user.UserModel;
import com.wevat.auth.data.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DemoData {

  @Autowired
  UserRepository repository;
  @Autowired
  BCryptPasswordEncoder encoder;

  @EventListener
  public void appReady(ApplicationReadyEvent event) {

    UserModel user1 = new UserModel();
    user1.setFirstName("Alan");
    user1.setUsername("alan");
    user1.setLastName("Partridge");
    user1.setPassword(encoder.encode("password"));
    user1.setPayoutCurrency("GBP");
    repository.save(user1);

    UserModel user2 = new UserModel();
    user2.setFirstName("Ron");
    user2.setLastName("Swanson");
    user2.setUsername("ron");
    user2.setPassword(encoder.encode("password"));
    user2.setPayoutCurrency("USD");
    repository.save(user2);

  }

}
