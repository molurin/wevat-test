
package com.wevat.rest.domain;


public enum ErrorCode {

    P00("generic.success"),
    P10001("generic.unsuccessful"),
    PH10003("payment.created"),
    P10005("invalid.field"),
    P10006("invalid.login"),
    P10007("successful.login"),
    P10010("invalid.token"),
    P10013("expired.token"),
    P10012("user.not.found");    
    

    private String message;

    ErrorCode(String message) {
        this.message = message;
    }

    public String message() {
        return this.message;
    }
}
