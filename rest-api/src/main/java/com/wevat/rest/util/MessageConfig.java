package com.wevat.rest.util;

import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:message.properties")
@ConfigurationProperties("")
public class MessageConfig {

  private final Map<String, String> message = new HashMap<>();

  public Map<String, String> getMessage() {
    return message;
  }

  public String getMessageFromCode(String code) {
    String msg = this.message.get(code);
    return msg;
  }

}
