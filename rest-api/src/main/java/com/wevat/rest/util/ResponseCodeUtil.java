package com.wevat.rest.util;

import com.wevat.rest.domain.ErrorCode;

public class ResponseCodeUtil {

  public static final ErrorCode GENERIC_SUCCESS = ErrorCode.P00;
  public static final ErrorCode GENERIC_ERROR = ErrorCode.P10001;
  public static final ErrorCode PAYMENT_CREATED = ErrorCode.PH10003;
  public static final ErrorCode INVALID_FIELD = ErrorCode.P10005;
  public static final ErrorCode INVALID_LOGIN_DETAILS = ErrorCode.P10006;
  public static final ErrorCode SUCCESSFULL_LOGIN = ErrorCode.P10007;
  public static final ErrorCode INVALID_TOKEN = ErrorCode.P10010;
  public static final ErrorCode EXPIRED_TOKEN_ERROR = ErrorCode.P10013;
  public static final ErrorCode USER_NOT_FOUND_ERROR = ErrorCode.P10012;

}
