package com.wevat.rest.exchange;

import com.wevat.core.exchange.BaseResponse;

public class CreatePaymentResponse extends BaseResponse {

  private Double paymentAmount;
  private String payoutCurrency;
  private String destinationCurrency;
  private Double destinationAmount;
  private Double rate;
  private Long paymentId;

  public Double getPaymentAmount() {
    return paymentAmount;
  }

  public void setPaymentAmount(Double paymentAmount) {
    this.paymentAmount = paymentAmount;
  }

  public String getPayoutCurrency() {
    return payoutCurrency;
  }

  public void setPayoutCurrency(String payoutCurrency) {
    this.payoutCurrency = payoutCurrency;
  }

  public String getDestinationCurrency() {
    return destinationCurrency;
  }

  public void setDestinationCurrency(String destinationCurrency) {
    this.destinationCurrency = destinationCurrency;
  }

  public Double getDestinationAmount() {
    return destinationAmount;
  }

  public void setDestinationAmount(Double destinationAmount) {
    this.destinationAmount = destinationAmount;
  }

  public Double getRate() {
    return rate;
  }

  public void setRate(Double rate) {
    this.rate = rate;
  }

  public Long getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(Long paymentId) {
    this.paymentId = paymentId;
  }

}
