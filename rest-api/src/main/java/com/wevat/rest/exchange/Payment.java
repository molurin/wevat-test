package com.wevat.rest.exchange;

import java.util.Date;

public class Payment {

  private Long id;
  private Long userId;
  private String payoutCurrency;
  private String destinationCurrency;
  private Double payoutAmount;
  private Double destinationAmount;
  private String quoteId;
  private Double rate;
  private Double fee;
  private String message;
  private String status;
  private Date expectedDeliveryDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getPayoutCurrency() {
    return payoutCurrency;
  }

  public void setPayoutCurrency(String payoutCurrency) {
    this.payoutCurrency = payoutCurrency;
  }

  public String getDestinationCurrency() {
    return destinationCurrency;
  }

  public void setDestinationCurrency(String destinationCurrency) {
    this.destinationCurrency = destinationCurrency;
  }

  public Double getPayoutAmount() {
    return payoutAmount;
  }

  public void setPayoutAmount(Double payoutAmount) {
    this.payoutAmount = payoutAmount;
  }

  public Double getDestinationAmount() {
    return destinationAmount;
  }

  public void setDestinationAmount(Double destinationAmount) {
    this.destinationAmount = destinationAmount;
  }

  public String getQuoteId() {
    return quoteId;
  }

  public void setQuoteId(String quoteId) {
    this.quoteId = quoteId;
  }

  public Double getRate() {
    return rate;
  }

  public void setRate(Double rate) {
    this.rate = rate;
  }

  public Double getFee() {
    return fee;
  }

  public void setFee(Double fee) {
    this.fee = fee;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Date getExpectedDeliveryDate() {
    return expectedDeliveryDate;
  }

  public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
    this.expectedDeliveryDate = expectedDeliveryDate;
  }

}
