package com.wevat.rest.exchange;

import com.wevat.core.exchange.BaseResponse;

public class GetPaymentResponse extends BaseResponse {

  private Payment payment;

  public Payment getPayment() {
    return payment;
  }

  public void setPayment(Payment payment) {
    this.payment = payment;
  }

}
