package com.wevat.rest.exchange;

public class CreatePaymentRequest {

  private Double paymentAmount;
  private String destinationCurrency;

  public Double getPaymentAmount() {
    return paymentAmount;
  }

  public void setPaymentAmount(Double paymentAmount) {
    this.paymentAmount = paymentAmount;
  }

  public String getDestinationCurrency() {
    return destinationCurrency;
  }

  public void setDestinationCurrency(String destinationCurrency) {
    this.destinationCurrency = destinationCurrency;
  }

}
