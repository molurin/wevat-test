package com.wevat.rest.exchange;

import com.wevat.core.exchange.BaseResponse;
import java.util.List;

public class GetUserPaymentsResponse extends BaseResponse{

  private List<Payment> payments;

  public List<Payment> getPayments() {
    return payments;
  }

  public void setPayments(List<Payment> payments) {
    this.payments = payments;
  }

}
