
package com.wevat.rest.api;

import com.wevat.rest.util.MessageConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BaseController {
    
    @Autowired
    MessageConfig messageConfig;
    
}
