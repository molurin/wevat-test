package com.wevat.rest.api;

import com.wevat.auth.service.UserService;
import com.wevat.core.domain.UserObject;
import com.wevat.core.exception.InvalidPropertyException;
import com.wevat.core.exception.UserNotFoundException;
import com.wevat.core.exchange.BaseResponse;
import com.wevat.core.util.AppUtil;
import com.wevat.payment.domain.Payment;
import com.wevat.payment.domain.PaymentResult;
import com.wevat.payment.domain.PaymentStatus;
import com.wevat.payment.exception.PaymentServiceException;
import com.wevat.payment.service.PaymentService;
import com.wevat.rest.exchange.CreatePaymentRequest;
import com.wevat.rest.exchange.CreatePaymentResponse;
import com.wevat.rest.exchange.GetPaymentResponse;
import com.wevat.rest.exchange.GetUserPaymentsResponse;
import com.wevat.rest.util.ResponseCodeUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payments")
public class PaymentController extends BaseController {

  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PaymentController.class);

  @Autowired
  PaymentService service;
  @Autowired
  UserService userService;

  @RequestMapping(value = "/create", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<BaseResponse> createUser(@RequestBody CreatePaymentRequest request, Authentication authentication) {
    CreatePaymentResponse response = new CreatePaymentResponse();
    HttpStatus httpStatus = null;
    try {

      Optional<UserObject> user = userService.getUserByUsername(authentication.getPrincipal().toString()); 
      if ( ! user.isPresent() ) {
        throw new UserNotFoundException();
      }

      // Create Payment Object
      Payment payment = new Payment.Builder()
          .sourceAmount(AppUtil.fromDecimal(request.getPaymentAmount()))
          .sourceCurrency(user.get().getPayoutCurrency())
          .targetCurrency(request.getDestinationCurrency())
          .userId(user.get().getId())
          .build();

      Optional<PaymentResult> result = service.createPayment(payment);
      if (result.isPresent()) {
        if (PaymentStatus.COMPLETED == result.get().getStatus()) {
          httpStatus = HttpStatus.OK;
          response.setDestinationAmount(AppUtil.toDecimal(result.get().getTargetAmount().orElse(0L)));
          response.setDestinationCurrency(result.get().getTargetCurrency().orElse(null));
          response.setPaymentAmount(AppUtil.toDecimal(result.get().getSourceAmount().orElse(0L)));
          response.setPayoutCurrency(result.get().getSourceCurrency().orElse(null));
          response.setRate(result.get().getRate().orElse(0D));
          response.setPaymentId(result.get().getId().orElse(0L));
          response.setCode(ResponseCodeUtil.GENERIC_SUCCESS.name());
          response.setMessage(messageConfig.getMessageFromCode(ResponseCodeUtil.PAYMENT_CREATED.message()));
        } else {
          httpStatus = HttpStatus.BAD_REQUEST;
          response.setCode(ResponseCodeUtil.GENERIC_ERROR.toString());
          response.setMessage(result.get().getMessage());
        }
      } else {
        httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        response.setCode(ResponseCodeUtil.GENERIC_ERROR.toString());
        response.setMessage(messageConfig.getMessageFromCode(ResponseCodeUtil.GENERIC_ERROR.message()));
      }

    } catch (InvalidPropertyException ex) {
      response.setCode(ResponseCodeUtil.GENERIC_ERROR.toString());
      response.setMessage(ex.getMessage());
      logger.debug(ex.getMessage());
      httpStatus = HttpStatus.BAD_REQUEST;
    } catch (UserNotFoundException ex) {
      response.setCode(ResponseCodeUtil.USER_NOT_FOUND_ERROR.toString());
      response.setMessage(messageConfig.getMessageFromCode(ResponseCodeUtil.USER_NOT_FOUND_ERROR.message()));
      logger.debug(ex.getMessage());
      httpStatus = HttpStatus.BAD_REQUEST;
    } catch (PaymentServiceException ex) {
      logger.debug(ex.getMessage());
      httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
      response.setCode(ResponseCodeUtil.GENERIC_ERROR.toString());
      response.setMessage(messageConfig.getMessageFromCode(ResponseCodeUtil.GENERIC_ERROR.message()));
    }

    return new ResponseEntity<>(response, httpStatus);
  }

  @RequestMapping(value = "/", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<BaseResponse> getUserPayments(Authentication authentication) {
    GetUserPaymentsResponse response = new GetUserPaymentsResponse();
    HttpStatus httpStatus = null;
    try {
      
      Optional<UserObject> user = userService.getUserByUsername(authentication.getPrincipal().toString()); 
      if ( ! user.isPresent() ) {
        throw new UserNotFoundException();
      }

      List<PaymentResult> result = service.getUserPayments(user.get().getId());
      List<com.wevat.rest.exchange.Payment> payments = new ArrayList<>();
      result.stream().map((paymentResult) -> {
        com.wevat.rest.exchange.Payment aPayment = new com.wevat.rest.exchange.Payment();
        aPayment.setDestinationAmount(AppUtil.toDecimal(paymentResult.getTargetAmount().orElse(0L)));
        aPayment.setDestinationCurrency(paymentResult.getTargetCurrency().orElse(null));
        aPayment.setExpectedDeliveryDate(paymentResult.getExpectedDeliveryDate().orElse(null));
        aPayment.setFee(AppUtil.toDecimal(paymentResult.getFee().orElse(0L)));
        aPayment.setId(paymentResult.getId().orElse(0L));
        aPayment.setMessage(paymentResult.getMessage());
        aPayment.setPayoutAmount(AppUtil.toDecimal(paymentResult.getSourceAmount().orElse(0L)));
        aPayment.setPayoutCurrency(paymentResult.getSourceCurrency().orElse(null));
        aPayment.setQuoteId(paymentResult.getQuoteId().orElse(null));
        aPayment.setRate(paymentResult.getRate().orElse(0D));
        aPayment.setStatus(paymentResult.getStatus().status());
        return aPayment;
      }).forEach((aPayment) -> {
        payments.add(aPayment);
      });
      response.setPayments(payments);

      httpStatus = HttpStatus.OK;
      response.setCode(ResponseCodeUtil.GENERIC_SUCCESS.name());
      response.setMessage(messageConfig.getMessageFromCode(ResponseCodeUtil.GENERIC_SUCCESS.message()));

    } catch (UserNotFoundException ex) {
      response.setCode(ResponseCodeUtil.USER_NOT_FOUND_ERROR.toString());
      response.setMessage(messageConfig.getMessageFromCode(ResponseCodeUtil.USER_NOT_FOUND_ERROR.message()));
      logger.debug(ex.getMessage());
      httpStatus = HttpStatus.BAD_REQUEST;
    } catch (PaymentServiceException ex) {
      response.setCode(ResponseCodeUtil.GENERIC_ERROR.toString());
      response.setMessage(ex.getMessage());
      logger.debug(ex.getMessage());
      httpStatus = HttpStatus.BAD_REQUEST;
    }

    return new ResponseEntity<>(response, httpStatus);
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<BaseResponse> getPayment(HttpServletRequest httpRequest, @PathVariable(value = "id") long id) {
    GetPaymentResponse response = new GetPaymentResponse();
    HttpStatus httpStatus = null;
    try {

      Optional<PaymentResult> result = service.getPayment(id);
      if (result.isPresent()) {
        PaymentResult paymentResult = result.get();
        com.wevat.rest.exchange.Payment aPayment = new com.wevat.rest.exchange.Payment();
        aPayment.setDestinationAmount(AppUtil.toDecimal(paymentResult.getTargetAmount().orElse(0L)));
        aPayment.setDestinationCurrency(paymentResult.getTargetCurrency().orElse(null));
        aPayment.setExpectedDeliveryDate(paymentResult.getExpectedDeliveryDate().orElse(null));
        aPayment.setFee(AppUtil.toDecimal(paymentResult.getFee().orElse(0L)));
        aPayment.setId(paymentResult.getId().orElse(0L));
        aPayment.setMessage(paymentResult.getMessage());
        aPayment.setPayoutAmount(AppUtil.toDecimal(paymentResult.getSourceAmount().orElse(0L)));
        aPayment.setPayoutCurrency(paymentResult.getSourceCurrency().orElse(null));
        aPayment.setQuoteId(paymentResult.getQuoteId().orElse(null));
        aPayment.setRate(paymentResult.getRate().orElse(0D));
        aPayment.setStatus(paymentResult.getStatus().status());

        response.setPayment(aPayment);
        httpStatus = HttpStatus.OK;
        response.setCode(ResponseCodeUtil.GENERIC_SUCCESS.name());
        response.setMessage(messageConfig.getMessageFromCode(ResponseCodeUtil.GENERIC_SUCCESS.message()));
      } else {
        httpStatus = HttpStatus.NOT_FOUND;
        response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
        response.setMessage(messageConfig.getMessageFromCode(ResponseCodeUtil.GENERIC_ERROR.message()));
      }

    } catch (PaymentServiceException ex) {
      response.setCode(ResponseCodeUtil.GENERIC_ERROR.toString());
      response.setMessage(ex.getMessage());
      logger.debug(ex.getMessage());
      httpStatus = HttpStatus.BAD_REQUEST;
    }

    return new ResponseEntity<>(response, httpStatus);
  } 

}
