package com.wevat.rest.api;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.wevat.core.exchange.BaseResponse;
import com.wevat.rest.util.MessageConfig;
import com.wevat.rest.util.ResponseCodeUtil;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.wevat.core.exchange.Error;
import javax.persistence.RollbackException;

@ControllerAdvice(annotations = RestController.class)
@ResponseBody
public class PaymentServiceApiAdvice {

  private Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  MessageConfig messageConfig;

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public BaseResponse handleValidationException(MethodArgumentNotValidException e) {
    BaseResponse response = new BaseResponse();
    response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
    response.setMessage("Bad request");
    BindingResult result = e.getBindingResult();
    List<FieldError> errorList = result.getFieldErrors();
    List<Error> errors = new ArrayList<>();
    for (FieldError fieldError : errorList) {
      errors.add(new Error(fieldError.getField(), fieldError.getDefaultMessage()));
    }
    response.setErrors(errors);
    return response;
  }

  @ExceptionHandler(RollbackException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public BaseResponse handleRollbackException(RollbackException e) {
    BaseResponse response = new BaseResponse();
    response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
    response.setMessage(e.getLocalizedMessage());
    if (e.getCause() != null) {
      String message = e.getCause().getMessage();

      response.setMessage(message);
    }
    return response;
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public BaseResponse handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
    BaseResponse response = new BaseResponse();
    response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
    response.setMessage(e.getLocalizedMessage());
    if (e.getCause() != null) {
      String message = e.getCause().getMessage();
      if (e.getCause() instanceof JsonMappingException) {
        String[] arr = message.split("\\(");
        if (arr.length > 0) {
          String temp = arr[0];
          String[] arr2 = message.split("\\[");
          if (arr2.length > 1) {
            message = temp + " (fieldName: [" + arr2[1];
          } else {
            message = temp;
          }
        }
      }

      if (e.getCause() instanceof JsonParseException) {
        String[] arr = message.split("at");
        if (arr.length > 0) {
          String temp = arr[0];
          JsonParseException jpe = (JsonParseException) e.getCause();
          message = temp + " [line: " + jpe.getLocation().getLineNr() + ", column: " + jpe.getLocation().getColumnNr() + "]";
        }
      }
      response.setMessage(message);
    }
    return response;
  }

  @ExceptionHandler(ServletRequestBindingException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public BaseResponse handleServletRequestBindingException(ServletRequestBindingException e) {
    BaseResponse response = new BaseResponse(ResponseCodeUtil.GENERIC_ERROR.name(), "Bad request");
    List<Error> errors = new ArrayList<>();
    errors.add(new Error("header", e.getMessage()));
    response.setErrors(errors);
    return response;
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public BaseResponse handleException(Exception e) {
    BaseResponse response = new BaseResponse();
    response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
    response.setMessage("Error processing request, please try again");
    logger.error(e.toString());
    e.printStackTrace();
    return response;
  }

  @ExceptionHandler(DateTimeParseException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ResponseBody
  public BaseResponse handleDateTimeParseException(DateTimeParseException e) {
    BaseResponse response = new BaseResponse();
    response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
    response.setMessage("Invalid date");
    logger.error(e.getMessage());
    return response;
  }

  @ExceptionHandler(DuplicateKeyException.class)
  @ResponseStatus(value = HttpStatus.CONFLICT)
  @ResponseBody
  public BaseResponse handleDuplicateKeyException(DuplicateKeyException e) {
    BaseResponse response = new BaseResponse();
    response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
    if (e.getRootCause() != null) {
      String message = e.getRootCause().getMessage();
      response.setMessage("Duplicate Exception. " + message.substring(message.indexOf("The")));
    } else {
      String message = e.getMostSpecificCause().getMessage();
      response.setMessage("Duplicate Exception. " + message.substring(message.indexOf("The")));
    }
    logger.error(e.getMessage());
    return response;
  }

  @ExceptionHandler(DataAccessException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ResponseBody
  public BaseResponse handleDataAccessException(DataAccessException e) {
    BaseResponse response = new BaseResponse();
    response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
    if (e.getRootCause() != null) {
      response.setMessage(e.getRootCause().getMessage());
    } else {
      response.setMessage(e.getMostSpecificCause().getMessage());
    }
    logger.error(e.getMessage());
    return response;
  }

  @ExceptionHandler(IllegalArgumentException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ResponseBody
  public BaseResponse handleIllegalArgumentException(IllegalArgumentException e) {
    BaseResponse response = new BaseResponse();
    response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
    response.setMessage(e.getMessage());
    return response;
  }

  @ExceptionHandler(InsufficientAuthenticationException.class)
  @ResponseStatus(value = HttpStatus.FORBIDDEN)
  @ResponseBody
  public BaseResponse handleInsufficientAuthenticationException(InsufficientAuthenticationException e) {
    BaseResponse response = new BaseResponse();
    response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
    response.setMessage(e.getMessage());
    return response;
  }

  @ExceptionHandler(AccessDeniedException.class)
  @ResponseStatus(value = HttpStatus.FORBIDDEN)
  @ResponseBody
  public BaseResponse handleAccessDeniedException(AccessDeniedException e) {
    BaseResponse response = new BaseResponse();
    response.setCode(ResponseCodeUtil.GENERIC_ERROR.name());
    response.setMessage(e.getMessage());
    return response;
  }

}
