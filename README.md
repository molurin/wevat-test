
#  FX Quote and Payment Service 

Service for booking exchange rates and payments

## Solution Design

### Technology Used

Technologies used are

#### Maven
Maven was used to manage dependencies and application packaging

#### Spring Boot
Spring boot was chosen to speed up development by providing automatic  Spring configurations and simple Maven Configurations. Spring boot also allows us to create stand-alone spring application with embedded Tomcat, therefore simplifying installation and enviroment dependencies. 

#### Spring Data + JPA + Hibernate
Spring data JPA provides CRUD capabilities to our model objects without the need of any boilerplate code. It abstracts datasource implementaions for us and make it easy to change datasource without changing code.

#### H2 DB

## Simple Design
The project was broken into sub-modules for code re-usability and easy management. Modules includes; 
Core Module, Authentication Module, Payment Service Module, Rest API Module (core, auth, payment-service and rest-api).


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. First you need to clone this repository.

### git clone git@bitbucket.org:molurin/wevat-test.git

### Prerequisites

You would need to setup Maven for building this project. https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html

### Building Project (Command Line)

Open your terminal and navigate to the 'wevat-test' folder that you just cloned. Run 'mvn install' 

### Run application using java -jar command

Run "java -jar rest-api/target/rest-api-0.0.1-SNAPSHOT.jar"

### Run application using Maven 

Enter rest-api folder and run "mvn spring-boot:run"


###You can also install as a init.d service
Copy the jar from rest-api/target/rest-api-0.0.1-SNAPSHOT.jar to /var/myapp/rest-api-0.0.1-SNAPSHOT.jar

$ sudo ln -s /var/myapp/rest-api-0.0.1-SNAPSHOT.jar /etc/init.d/restapi
$ service transferservice start


## REST API

### Sample Data
The following users are already pre-populated for your testing purpose

#### User 1
FirstName : Alan, Last Name : Partridge, Payout Currency : GBP, Username : alan, Password : password 

#### User 2
FirstName : Ron, Last Name : Swanson, Payout Currency : USD, Username : ron, Password : password 

### Authentication - JWT

To call API you need to login and get JWT token.

POST http://localhost:8080/login
{"username":"alan","password":"password"}

Copy out the accessToken in response and use in header of every other requests.

Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyb24iLCJleHAiOjE1NDE5NTQxNzIsImFjY2VzcyI6W119.2v5ZHLtXu0wbM2yIVefJeNL8Wfk_0Jp5Zz5etzL7fS1YgIPiMMtKD20lTlpqz8xgXG1CvUy0BPhKdvZtMBOSzw

### Create Quote

http://localhost:8080/payments/create
{"paymentAmount":100,"destinationCurrency": "USD"}


### Get User Payments

GET http://localhost:7021/payments/

### Get Payment By Id

GET http://localhost:7021/payments/3








