package com.wevat.auth.service.impl;

import com.wevat.auth.data.user.UserModel;
import com.wevat.auth.data.user.UserRepository;
import com.wevat.auth.service.UserService;
import com.wevat.core.domain.UserObject;
import com.wevat.core.exception.InvalidPropertyException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

public class UserServiceTest {

  private static final String sourceCurrency = "GBP";
  private static final String firstName = "Alan";
  private static final Long userId = 1L;
  private static final String lastName = "Geek";
  private static final String username = "alan";

  @InjectMocks
  UserService service = new UserServiceImpl();
  @Mock
  private UserRepository repository;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getUserByIdTest() {
    try {
      when(repository.getById(userId)).thenReturn(provideUserModel());
      Optional<UserObject> result = service.getUserById(userId);
      assertTrue(result.isPresent());
      assertEquals(result.get().getFirstName(), firstName);
      assertEquals(result.get().getLastName(), lastName);
      assertEquals(result.get().getUsername(), username);
      assertEquals(result.get().getPayoutCurrency(), sourceCurrency);

    } catch (Exception ex) {
      Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Test
  public void getUserByIdIdNotFoundTest() {
    try {
      when(repository.getById(userId)).thenReturn(null);
      Optional<UserObject> result = service.getUserById(userId);
      assertFalse(result.isPresent());

    } catch (Exception ex) {
      Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Test
  public void getUserByUsernameTest() {
    try {
      when(repository.getByUsername(username)).thenReturn(provideUserModel());
      Optional<UserObject> result = service.getUserByUsername(username);
      assertTrue(result.isPresent());
      assertEquals(result.get().getFirstName(), firstName);
      assertEquals(result.get().getLastName(), lastName);
      assertEquals(result.get().getUsername(), username);
      assertEquals(result.get().getPayoutCurrency(), sourceCurrency);

    } catch (Exception ex) {
      Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Test
  public void getUserByUsernameNotFoundTest() {
    try {
      when(repository.getById(userId)).thenReturn(null);
      Optional<UserObject> result = service.getUserByUsername(username);
      assertFalse(result.isPresent());

    } catch (Exception ex) {
      Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private UserModel provideUserModel() throws InvalidPropertyException {
    UserModel user = new UserModel();
    user.setFirstName(firstName);
    user.setId(userId);
    user.setLastName(lastName);
    user.setPayoutCurrency(sourceCurrency);
    user.setUsername(username);

    return user;
  }

}
