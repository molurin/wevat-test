package com.wevat.auth.exchange;

import com.wevat.core.exchange.BaseResponse;

public class LoginResponse extends BaseResponse {

  private String accessToken;
  private long expiresIn;
  private long expiryTime;
  private String firstName;
  private String lastName;
  private String payoutCurrency;

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public long getExpiresIn() {
    return expiresIn;
  }

  public void setExpiresIn(long expiresIn) {
    this.expiresIn = expiresIn;
  }

  public long getExpiryTime() {
    return expiryTime;
  }

  public void setExpiryTime(long expiryTime) {
    this.expiryTime = expiryTime;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPayoutCurrency() {
    return payoutCurrency;
  }

  public void setPayoutCurrency(String payoutCurrency) {
    this.payoutCurrency = payoutCurrency;
  }

}
