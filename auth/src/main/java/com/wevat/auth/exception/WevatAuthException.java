package com.wevat.auth.exception;

public class WevatAuthException extends Exception {

  private int code;

  /**
   *
   */
  public WevatAuthException() {
    super();
  }

  /**
   * @param message
   */
  public WevatAuthException(String message) {
    super(message);
  }

  public WevatAuthException(int code, String locale) {
    this.code = code;
  }

  public WevatAuthException(int code, String[] params, String locale) {
    this.code = code;
  }

  public WevatAuthException(int code, String locale, Throwable cause) {
    this.code = code;
  }

  /**
   * @param message
   * @param cause
   */
  public WevatAuthException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * @param cause
   */
  public WevatAuthException(Throwable cause) {
    super(cause);
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

}
