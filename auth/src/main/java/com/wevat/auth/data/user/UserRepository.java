package com.wevat.auth.data.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Long> {

  UserModel getByUsername(String username);
  UserModel getById(long id);

}
