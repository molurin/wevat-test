package com.wevat.auth.service.impl;

import com.wevat.auth.service.UserService;
import com.wevat.core.domain.UserObject;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  UserService userService;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
    Optional<UserObject> user = getUserObject(username);

    if (!user.isPresent()) {

      return new org.springframework.security.core.userdetails.User("", "", grantedAuthorities);
    }

    return new org.springframework.security.core.userdetails.User(String.valueOf(user.get().getUsername()), user.get().getPassword().orElse(""), grantedAuthorities);
  }

  @Transactional
  public Optional<UserObject> getUserObject(String username) {
    return userService.getUserByUsername(username);
  }

  public Set<GrantedAuthority> getAuthorities(List<String> userAccess) {
    Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
    if (userAccess != null) {
      userAccess.stream().forEach((role) -> {
        grantedAuthorities.add(new SimpleGrantedAuthority(role));
      });
    }
    return grantedAuthorities;
  }

}
