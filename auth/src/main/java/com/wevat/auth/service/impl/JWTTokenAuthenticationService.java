package com.wevat.auth.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wevat.core.exchange.BaseResponse;
import com.wevat.core.exchange.ErrorResponse;
import com.wevat.auth.exchange.LoginResponse;
import com.wevat.auth.exchange.ApiCredentials;
import com.wevat.auth.service.WevatAuthenticationService;
import com.wevat.core.domain.UserObject;
import com.wevat.core.util.AppUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.IOException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import static java.util.Collections.emptyList;
import java.util.Optional;
import javax.servlet.ServletException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
@Primary
public class JWTTokenAuthenticationService implements WevatAuthenticationService{

  @Autowired
  UserDetailsServiceImpl userDetailsService;


  static final long EXPIRATIONTIME = 600000; //864_000_000; // 10 days
  static final String SECRET = "Secret_Key"; //TODO: move to config
  static final String TOKEN_PREFIX = "Bearer";
  static final String HEADER_STRING = "Authorization";

  @Override
  public ApiCredentials getCredientials(HttpServletRequest req) throws AuthenticationException, IOException, ServletException {
    ApiCredentials creds = new ObjectMapper()
        .readValue(req.getInputStream(), ApiCredentials.class);

    return creds;
  }

  @Override
  public void addAuthentication(HttpServletResponse res, Authentication auth) throws IOException, ServletException {
    LoginResponse loginResponse = new LoginResponse();
    Optional<UserObject> user = userDetailsService.getUserObject(auth.getName());
    Date expiresAt = new Date(System.currentTimeMillis() + EXPIRATIONTIME);
    Claims claims = Jwts.claims().setSubject(auth.getName());
    claims.setExpiration(expiresAt);
    if ( user.isPresent() ) {

      claims.put("access", auth.getAuthorities());
      loginResponse.setPayoutCurrency(user.get().getPayoutCurrency());
      loginResponse.setFirstName(user.get().getFirstName());
      loginResponse.setLastName(user.get().getLastName());
    }

    String JWT = Jwts.builder()
        .setSubject(auth.getName())
        .setExpiration(expiresAt)
        .signWith(SignatureAlgorithm.HS512, SECRET)
        .setClaims(claims)
        .compact();
    res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);

    loginResponse.setExpiresIn(EXPIRATIONTIME);
    loginResponse.setAccessToken(JWT);
    loginResponse.setExpiryTime(expiresAt.getTime());
    loginResponse.setCode("00");
    loginResponse.setMessage("Success");

    byte[] responseToSend = restResponseBytes(loginResponse);
    res.setHeader("Content-Type", "application/json");
    res.setStatus(HttpServletResponse.SC_OK);
    res.getOutputStream().write(responseToSend);

  }

  @Override
  public Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    String token = request.getHeader(HEADER_STRING);
    Authentication authentication = null;
    if ( token != null ) {
      try {
        // parse the token.
        Claims body = Jwts.parser()
            .setSigningKey(SECRET)
            .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
            .getBody();
        String username = body.getSubject();
        
        authentication = ! AppUtil.isEmptyString(username)
            ? new UsernamePasswordAuthenticationToken(username, null, emptyList())
            : null;
                
      } catch (ExpiredJwtException e) {
        setExpiredTokenError(response);
      }

    }
    return authentication;
  }

//  @Override
//  public Authentication getAuthentication(String token) {
//    UserDetails userDetails = userDetailsService.loadUserByUsername(token);
//    return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
//  }

  @Override
  public void setAuthenticationError(HttpServletResponse res) throws IOException, ServletException {

    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setCode("1000"); // TODO: Proper Response Code Mapping
    errorResponse.setMessage("Login Failure");

    byte[] responseToSend = restResponseBytes(errorResponse);
    res.setHeader("Content-Type", "application/json");
    res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    res.getOutputStream().write(responseToSend);

  }

  public void setExpiredTokenError(HttpServletResponse res) throws IOException, ServletException {

    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setCode("1000"); // TODO: Proper Response Code Mapping
    errorResponse.setMessage("Expired Token");

    byte[] responseToSend = restResponseBytes(errorResponse);
    res.setHeader("Content-Type", "application/json");
    res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    res.getOutputStream().write(responseToSend);

  }

  private byte[] restResponseBytes(BaseResponse response) throws IOException {
    String serialized = new ObjectMapper().writeValueAsString(response);
    return serialized.getBytes();
  }
}
