package com.wevat.auth.service;

import com.wevat.auth.exchange.ApiCredentials;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public interface WevatAuthenticationService {

  ApiCredentials getCredientials(HttpServletRequest req) throws AuthenticationException, IOException, ServletException;

  void addAuthentication(HttpServletResponse res, Authentication auth) throws IOException, ServletException;

  Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;

  void setAuthenticationError(HttpServletResponse res) throws IOException, ServletException;

}
