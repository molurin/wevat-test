package com.wevat.auth.service;

import com.wevat.core.domain.UserObject;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

  Optional<UserObject> getUserByUsername(String username);
  Optional<UserObject> getUserById(long userId);

}
