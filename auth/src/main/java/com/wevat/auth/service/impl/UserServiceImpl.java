package com.wevat.auth.service.impl;

import com.wevat.auth.data.user.UserModel;
import com.wevat.auth.data.user.UserRepository;
import com.wevat.core.exception.InvalidPropertyException;
import com.wevat.auth.service.UserService;
import com.wevat.core.domain.UserObject;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Primary
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository repository;

  @Transactional
  @Override
  public Optional<UserObject> getUserById(long userId) {
    try {
      return fromUserModel(repository.getById(userId));
    } catch (InvalidPropertyException ex) {
      Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }

    return Optional.empty();
  }
  
  @Override
  public Optional<UserObject> getUserByUsername(String username) {
    try {
      return fromUserModel(repository.getByUsername(username));
    } catch (InvalidPropertyException ex) {
      Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }

    return Optional.empty();
  }

  private Optional<UserObject> fromUserModel(UserModel userModel) throws InvalidPropertyException {
    if (null == userModel) {
      return Optional.empty();
    }

    UserObject user = new UserObject.Builder()
        .firstName(userModel.getFirstName())
        .lastName(userModel.getLastName())
        .id(userModel.getId())
        .payoutCurrency(userModel.getPayoutCurrency())
        .username(userModel.getUsername())
        .password(userModel.getPassword())
        .build();

    return Optional.ofNullable(user);
  }

}
