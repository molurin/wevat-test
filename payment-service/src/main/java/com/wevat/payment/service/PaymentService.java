package com.wevat.payment.service;

import com.wevat.payment.domain.Payment;
import com.wevat.payment.domain.PaymentResult;
import com.wevat.payment.exception.PaymentServiceException;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public interface PaymentService {

  Optional<PaymentResult> createPayment(Payment obj) throws PaymentServiceException;
  
  List<PaymentResult> getUserPayments(Long userId) throws PaymentServiceException;
  
  Optional<PaymentResult> getPayment(Long id) throws PaymentServiceException;

}
