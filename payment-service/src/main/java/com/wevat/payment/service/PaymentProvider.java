package com.wevat.payment.service;

import com.wevat.payment.domain.Payment;
import com.wevat.payment.domain.PaymentResult;
import com.wevat.payment.exception.PaymentServiceException;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public interface PaymentProvider {

  Optional<PaymentResult> createExchangeRateQuote(Payment payment, Long paymentId) throws PaymentServiceException;

}
