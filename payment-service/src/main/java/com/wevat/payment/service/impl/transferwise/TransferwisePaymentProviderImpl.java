package com.wevat.payment.service.impl.transferwise;

import com.wevat.core.client.rest.RestClient;
import com.wevat.core.exception.InvalidPropertyException;
import com.wevat.core.exception.WevatException;
import com.wevat.core.util.AppUtil;
import com.wevat.payment.domain.Payment;
import com.wevat.payment.domain.PaymentResult;
import com.wevat.payment.domain.PaymentStatus;
import com.wevat.payment.exception.PaymentServiceException;
import com.wevat.payment.exchange.GetQuoteRequest;
import com.wevat.payment.exchange.GetQuoteResponse;
import com.wevat.payment.service.PaymentProvider;
import com.wevat.payment.util.PaymentServiceUtil;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Primary
public class TransferwisePaymentProviderImpl implements PaymentProvider {

  @Autowired
  RestClient client;
  

  @Override
  public Optional<PaymentResult> createExchangeRateQuote(Payment payment, Long paymentId) throws PaymentServiceException {
    GetQuoteRequest request = getGetQuoteRequest(payment);
    HttpHeaders headers = AppUtil.getAuthorizationHeader(AppUtil.getCommonHttpHeader(), PaymentServiceUtil.TRANSFERWISE_API_TOKEN);
    try {
      ResponseEntity<GetQuoteResponse> responseEntity = client.sendPOST(getEndpoint(), request, headers, GetQuoteResponse.class);
      if ( null != responseEntity && responseEntity.getStatusCode() == HttpStatus.OK ) {
        return Optional.ofNullable(parseResponse(responseEntity.getBody(), payment, PaymentStatus.COMPLETED, paymentId));
      } else if ( null != responseEntity && responseEntity.hasBody() ) {
        return Optional.ofNullable(parseResponse(responseEntity.getBody(), payment, PaymentStatus.FAILED, paymentId));
      }
    
    } catch (WevatException | InvalidPropertyException ex) {
      Logger.getLogger(TransferwisePaymentProviderImpl.class.getName()).log(Level.SEVERE, null, ex);
      throw new PaymentServiceException(ex);
    }
    return Optional.empty();
  }

  private GetQuoteRequest getGetQuoteRequest(Payment payment) throws PaymentServiceException {

    GetQuoteRequest request = new GetQuoteRequest();
    request.setProfile(PaymentServiceUtil.TRANSFERWISE_PROFILE_ID); 
    request.setRateType(PaymentServiceUtil.TRANSFERWISE_RATE_TYPE);
    request.setSource(payment.getSourceCurrency());
    request.setSourceAmount(AppUtil.toDecimal(payment.getSourceAmount().orElse(0L)));
    request.setTarget(payment.getTargetCurrency());

    return request;
  }

  private PaymentResult parseResponse(GetQuoteResponse response, Payment payment, PaymentStatus status, Long paymentId) throws InvalidPropertyException {
    if (null == response) {
      return null;
    }
    
    String message = response.getMessage();
    if ( AppUtil.isEmptyString(message) ) {
      StringBuilder str = new StringBuilder("");
      if ( null != response.getErrors() && response.getErrors().size() > 0) {
        response.getErrors().stream().forEach((error) -> {
          str.append(error.getMessage()).append(" , ");
        });
      }
      message = str.toString();
    }
    
    if ( AppUtil.isEmptyString(message) ) 
      message = PaymentStatus.COMPLETED ==  status ? "Success" : "Payment Failed";
    
    return new PaymentResult.Builder()
        .id(paymentId)
        .fee(AppUtil.fromDecimal(response.getFee()))
        .rate(response.getRate())
        .sourceAmount(AppUtil.fromDecimal(response.getSourceAmount()))
        .sourceCurrency(response.getSource())
        .targetAmount(AppUtil.fromDecimal(response.getTargetAmount()))
        .targetCurrency(response.getTarget())
        .userId(payment.getUserId())
        .quoteId(String.valueOf(response.getId()))
        .rate(response.getRate())
        .fee(AppUtil.fromDecimal(response.getFee()))
        .expectedDeliveryDate(response.getDeliveryEstimate())
        .status(status)
        .message(message)
        .build();
  }

  private String getEndpoint() {
    return PaymentServiceUtil.TRANSFERWISE_URL.concat("/").concat("quotes");
  }

}
