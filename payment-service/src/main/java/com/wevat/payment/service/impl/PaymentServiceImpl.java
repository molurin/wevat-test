package com.wevat.payment.service.impl;

import com.wevat.core.exception.InvalidPropertyException;
import com.wevat.payment.data.paymentlog.Payments;
import com.wevat.payment.data.paymentlog.PaymentsRepository;
import com.wevat.payment.domain.Payment;
import com.wevat.payment.domain.PaymentStatus;
import com.wevat.payment.exception.PaymentServiceException;
import com.wevat.payment.domain.PaymentResult;
import com.wevat.payment.service.PaymentProvider;
import com.wevat.payment.service.PaymentService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentServiceImpl implements PaymentService {
  
  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);

  @Autowired
  PaymentsRepository paymentsRepository;
  @Autowired
  PaymentProvider exchangeRateService;

  @Override
  public Optional<com.wevat.payment.domain.PaymentResult> createPayment(Payment obj) throws PaymentServiceException {
    // Log new Payment
    Payments newPayment = logPayment(obj);
    try {
      // Call remote processor
      if ( null != newPayment ) {
        Optional<PaymentResult> paymentResult = exchangeRateService.createExchangeRateQuote(obj, newPayment.getId());
        if ( paymentResult.isPresent() ) {

          // Update payment log
          newPayment.setPayoutAmount(paymentResult.get().getSourceAmount().orElse(0L));
          newPayment.setDestinationAmount(paymentResult.get().getTargetAmount().orElse(0L));
          newPayment.setQuoteId(String.valueOf(paymentResult.get().getQuoteId().orElse(null)));
          newPayment.setPaymentStatus(paymentResult.get().getStatus().status());
          newPayment.setRate(paymentResult.get().getRate().orElse(0D));
          newPayment.setFee(paymentResult.get().getFee().orElse(0L));
          newPayment.setDeliveryEstimate(paymentResult.get().getExpectedDeliveryDate().orElse(null));
          newPayment.setHostResponseMessage(paymentResult.get().getMessage());
          updatePaymentLog(newPayment);

          return paymentResult;
        }
      }

    } catch (PaymentServiceException ex) {
      if (null != newPayment) {
        newPayment.setPaymentStatus(PaymentStatus.FAILED.status());
        updatePaymentLog(newPayment);
      }

      Logger.getLogger(PaymentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
      throw new PaymentServiceException(ex.getMessage());
    }
    return Optional.empty();
  }

  @Override
  public List<PaymentResult> getUserPayments(Long userId) throws PaymentServiceException {
    List<PaymentResult> userPayments = new ArrayList<>();
    List<Payments> payments = paymentsRepository.findByUserId(userId);

    payments.stream().forEach((aPayment) -> {
      try {
        userPayments.add(toPaymentResult(aPayment));
      } catch (InvalidPropertyException ex) {
        Logger.getLogger(PaymentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
      }
    });

    return userPayments;
  }

  @Override
  public Optional<PaymentResult> getPayment(Long id) throws PaymentServiceException {
    try {
      return Optional.ofNullable(toPaymentResult(paymentsRepository.getOne(id)));
    } catch (InvalidPropertyException ex) {
      Logger.getLogger(PaymentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
      throw new PaymentServiceException(ex);
    }
  }

  private Payments logPayment(Payment payment) throws PaymentServiceException {
    Payments newPayment = new Payments();
    try{
    newPayment.setDestinationAmount(payment.getTargetAmount().orElse(0L));
    newPayment.setDestinationCurrency(payment.getTargetCurrency());
    newPayment.setPaymentStatus(PaymentStatus.PENDING.status());
    newPayment.setPayoutCurrency(payment.getSourceCurrency());
    newPayment.setPayoutAmount(payment.getSourceAmount().orElse(0L));
    newPayment.setUserId(payment.getUserId());
    newPayment = paymentsRepository.save(newPayment);
    }catch (Exception ex) {
      logger.debug(ex.getMessage());
      throw new PaymentServiceException(ex);
    }
    
    return newPayment;
  }

  private void updatePaymentLog(Payments payment) {
    paymentsRepository.save(payment);
  }
  
  private PaymentResult toPaymentResult(Payments payment) throws InvalidPropertyException {
    if ( null == payment ) return null;
    return new PaymentResult.Builder()
        .id(payment.getId())
        .fee(payment.getFee())
        .rate(payment.getRate())
        .sourceAmount(payment.getPayoutAmount())
        .sourceCurrency(payment.getPayoutCurrency())
        .targetAmount(payment.getDestinationAmount())
        .targetCurrency(payment.getDestinationCurrency())
        .userId(payment.getUserId())
        .quoteId(payment.getQuoteId())
        .expectedDeliveryDate(payment.getDeliveryEstimate())
        .status(PaymentStatus.valueOf(payment.getPaymentStatus()))
        .message(payment.getHostResponseMessage())
        .build();
  }

}
