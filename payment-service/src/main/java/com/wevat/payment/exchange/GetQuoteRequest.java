package com.wevat.payment.exchange;

public class GetQuoteRequest {

  private String profile;
  private String source;
  private String target;
  private String rateType;
  private Double sourceAmount;

  public String getProfile() {
    return profile;
  }

  public void setProfile(String profile) {
    this.profile = profile;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public String getRateType() {
    return rateType;
  }

  public void setRateType(String rateType) {
    this.rateType = rateType;
  }

  public Double getSourceAmount() {
    return sourceAmount;
  }

  public void setSourceAmount(Double sourceAmount) {
    this.sourceAmount = sourceAmount;
  }

  @Override
  public String toString() {
    return "GetQuoteRequest{" + "profile=" + profile + ", source=" + source + ", target=" + target + ", rateType=" + rateType + ", sourceAmount=" + sourceAmount + '}';
  }
 
    

}
