package com.wevat.payment.exchange;

import com.wevat.core.exchange.BaseResponse;
import java.util.Date;

public class GetQuoteResponse extends BaseResponse{

  private Long id;
  private String source;
  private String target;
  private double sourceAmount;
  private double targetAmount;
  private String type;
  private double rate;
  private Date createdTime;
  private String createdByUserId;
  private String profile;
  private String rateType;
  private Date deliveryEstimate;
  private double fee;
  private String[] allowedProfileTypes;
  private Boolean ofSourceAmount;
  private Boolean guaranteedTargetAmount;

  

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public double getSourceAmount() {
    return sourceAmount;
  }

  public void setSourceAmount(double sourceAmount) {
    this.sourceAmount = sourceAmount;
  }

  public double getTargetAmount() {
    return targetAmount;
  }

  public void setTargetAmount(double targetAmount) {
    this.targetAmount = targetAmount;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public double getRate() {
    return rate;
  }

  public void setRate(double rate) {
    this.rate = rate;
  }

  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }

  public String getCreatedByUserId() {
    return createdByUserId;
  }

  public void setCreatedByUserId(String createdByUserId) {
    this.createdByUserId = createdByUserId;
  }

  public String getProfile() {
    return profile;
  }

  public void setProfile(String profile) {
    this.profile = profile;
  }

  public String getRateType() {
    return rateType;
  }

  public void setRateType(String rateType) {
    this.rateType = rateType;
  }

  public Date getDeliveryEstimate() {
    return deliveryEstimate;
  }

  public void setDeliveryEstimate(Date deliveryEstimate) {
    this.deliveryEstimate = deliveryEstimate;
  }

  public double getFee() {
    return fee;
  }

  public void setFee(double fee) {
    this.fee = fee;
  }

  public Boolean getOfSourceAmount() {
    return ofSourceAmount;
  }

  public void setOfSourceAmount(Boolean ofSourceAmount) {
    this.ofSourceAmount = ofSourceAmount;
  }

  public Boolean getGuaranteedTargetAmount() {
    return guaranteedTargetAmount;
  }

  public void setGuaranteedTargetAmount(Boolean guaranteedTargetAmount) {
    this.guaranteedTargetAmount = guaranteedTargetAmount;
  }

  public String[] getAllowedProfileTypes() {
    return allowedProfileTypes;
  }

  public void setAllowedProfileTypes(String[] allowedProfileTypes) {
    this.allowedProfileTypes = allowedProfileTypes;
  }

  @Override
  public String toString() {
    return "GetQuoteResponse{" + "id=" + id + ", source=" + source + ", target=" + target + ", sourceAmount=" + sourceAmount + ", targetAmount=" + targetAmount + ", type=" + type + ", rate=" + rate + ", createdTime=" + createdTime + ", createdByUserId=" + createdByUserId + ", profile=" + profile + ", rateType=" + rateType + ", deliveryEstimate=" + deliveryEstimate + ", fee=" + fee + ", allowedProfileTypes=" + allowedProfileTypes + ", ofSourceAmount=" + ofSourceAmount + ", guaranteedTargetAmount=" + guaranteedTargetAmount + '}';
  }
  

}
