package com.wevat.payment.data.paymentlog;

import com.wevat.core.data.BaseModel;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "payment_log")
public class Payments extends BaseModel implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  @Min(1)
  private long userId;
  @NotBlank
  private String destinationCurrency;
  @NotBlank
  private String payoutCurrency;
  private long payoutAmount;
  private long destinationAmount;
  private double rate;
  private long fee;
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date deliveryEstimate;
  private String quoteId;
  private String paymentStatus;
  private String hostResponseCode;
  private String hostResponseMessage;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getDestinationCurrency() {
    return destinationCurrency;
  }

  public void setDestinationCurrency(String destinationCurrency) {
    this.destinationCurrency = destinationCurrency;
  }

  public String getPayoutCurrency() {
    return payoutCurrency;
  }

  public void setPayoutCurrency(String payoutCurrency) {
    this.payoutCurrency = payoutCurrency;
  }

  public long getPayoutAmount() {
    return payoutAmount;
  }

  public void setPayoutAmount(long payoutAmount) {
    this.payoutAmount = payoutAmount;
  }

  public long getDestinationAmount() {
    return destinationAmount;
  }

  public void setDestinationAmount(long destinationAmount) {
    this.destinationAmount = destinationAmount;
  }

  public String getQuoteId() {
    return quoteId;
  }

  public void setQuoteId(String quoteId) {
    this.quoteId = quoteId;
  }

  public String getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(String paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public String getHostResponseCode() {
    return hostResponseCode;
  }

  public void setHostResponseCode(String hostResponseCode) {
    this.hostResponseCode = hostResponseCode;
  }

  public String getHostResponseMessage() {
    return hostResponseMessage;
  }

  public void setHostResponseMessage(String hostResponseMessage) {
    this.hostResponseMessage = hostResponseMessage;
  }

  public double getRate() {
    return rate;
  }

  public void setRate(double rate) {
    this.rate = rate;
  }

  public long getFee() {
    return fee;
  }

  public void setFee(long fee) {
    this.fee = fee;
  }

  public Date getDeliveryEstimate() {
    return deliveryEstimate;
  }

  public void setDeliveryEstimate(Date deliveryEstimate) {
    this.deliveryEstimate = deliveryEstimate;
  }

}
