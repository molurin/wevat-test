package com.wevat.payment.data.paymentlog;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentsRepository extends JpaRepository<Payments, Long> {

  List<Payments> findByUserId(long userId);

}
