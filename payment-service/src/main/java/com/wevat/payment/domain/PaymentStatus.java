package com.wevat.payment.domain;

public enum PaymentStatus {

  PENDING("PENDING"),
  COMPLETED("COMPLETED"),
  FAILED("FAILED");

  private final String status;

  PaymentStatus(String status) {
    this.status = status;
  }

  public String status() {
    return this.status;
  }
}
