package com.wevat.payment.domain;

import com.wevat.core.exception.InvalidPropertyException;
import com.wevat.core.util.AppUtil;
import java.util.Optional;

public class Payment {
  
  private final Long id;
  private final Long userId;
  private final String sourceCurrency;
  private final String targetCurrency;
  private final Long sourceAmount;
  private final Long targetAmount;
  private final String quoteId;

  private Payment(Builder builder) {
    this.id = builder.id;
    this.userId = builder.userId;
    this.sourceCurrency = builder.sourceCurrency;
    this.targetCurrency = builder.targetCurrency;
    this.sourceAmount = builder.sourceAmount;
    this.targetAmount = builder.targetAmount;
    this.quoteId = builder.quoteId;
  }

  public static class Builder {
    private Long id;
    private Long userId;
    private String sourceCurrency;
    private String targetCurrency;
    private Long sourceAmount;
    private Long targetAmount;
    private String quoteId;

    public Builder id(Long id) {
      this.id = id;
      return this;
    }
    
    public Builder userId(Long userId) {
      this.userId = userId;
      return this;
    }

    public Builder sourceCurrency(String sourceCurrency) {
      this.sourceCurrency = sourceCurrency;
      return this;
    }

    public Builder targetCurrency(String targetCurrency) {
      this.targetCurrency = targetCurrency;
      return this;
    }

    public Builder sourceAmount(Long sourceAmount) {
      this.sourceAmount = sourceAmount;
      return this;
    }

    public Builder targetAmount(Long targetAmount) {
      this.targetAmount = targetAmount;
      return this;
    }

    public Builder quoteId(String quoteId) {
      this.quoteId = quoteId;
      return this;
    }

    public Payment build() throws InvalidPropertyException {

      Payment obj = new Payment(this);
      validate(obj);
      return obj;
    }
  }

  public Optional<Long> getId() {
    return Optional.ofNullable(id);
  }

  public Long getUserId() {
    return userId;
  }

  public String getSourceCurrency() {
    return sourceCurrency;
  }

  public String getTargetCurrency() {
    return targetCurrency;
  }

  public Optional<Long> getSourceAmount() {
    return Optional.ofNullable(sourceAmount);
  }

  public Optional<Long> getTargetAmount() {
    return Optional.ofNullable(targetAmount);
  }

  public Optional<String> getQuoteId() {
    return Optional.ofNullable(quoteId);
  }

  @Override
  public String toString() {
    return "Payment{" + "sourceCurrency=" + sourceCurrency + ", targetCurrency=" + targetCurrency + ", sourceAmount=" + sourceAmount + ", targetAmount=" + targetAmount + ", quoteId=" + quoteId + '}';
  }

  private static void validate(Payment obj) throws InvalidPropertyException {
    if (null == obj) {
      throw new InvalidPropertyException("Null object");
    }else if (null == obj.getUserId() || obj.getUserId() < 1 ) {
      throw new InvalidPropertyException("Invalid Object. User Id not found.");
    } else if ( AppUtil.isEmptyString(obj.getSourceCurrency()) ) {
      throw new InvalidPropertyException("Invalid Object. Source Currency must not be empty.");
    }  else if ( AppUtil.isEmptyString(obj.getTargetCurrency()) ) {
      throw new InvalidPropertyException("Invalid Object. Target Currency must not be empty.");
    }else if ( ! obj.getSourceAmount().isPresent() && ! obj.getTargetAmount().isPresent() ) {
      throw new InvalidPropertyException("Invalid Object. Either Source Amount or Target Amount must be present.");
    }
  }
}
