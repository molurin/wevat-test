package com.wevat.payment.domain;

import com.wevat.core.exception.InvalidPropertyException;
import com.wevat.core.util.AppUtil;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.annotation.Immutable;

@Immutable
public class PaymentResult {

  private final Long id;
  private final Long userId;
  private final String sourceCurrency;
  private final String targetCurrency;
  private final Long sourceAmount;
  private final Long targetAmount;
  private final String quoteId;
  private final Double rate;
  private final Long fee;
  private final String message;
  private final PaymentStatus status;
  private final Date expectedDeliveryDate;
  
  

  private PaymentResult(Builder builder) {
    this.id = builder.id;
    this.userId = builder.userId;
    this.sourceCurrency = builder.sourceCurrency;
    this.targetCurrency = builder.targetCurrency;
    this.sourceAmount = builder.sourceAmount;
    this.targetAmount = builder.targetAmount;
    this.rate = builder.rate;
    this.fee = builder.fee;
    this.message = builder.message;
    this.quoteId = builder.quoteId;
    this.status = builder.status;
    this.expectedDeliveryDate = builder.expectedDeliveryDate;
  }

  public static class Builder {

    private Long id;
    private Long userId;
    private String sourceCurrency;
    private String targetCurrency;
    private Long sourceAmount;
    private Long targetAmount;
    private Double rate;
    private Long fee;
    private String message;
    private String quoteId;
    private PaymentStatus status;
    private Date expectedDeliveryDate;

    public Builder id(Long id) {
      this.id = id;
      return this;
    }

    public Builder userId(Long userId) {
      this.userId = userId;
      return this;
    }

    public Builder sourceCurrency(String sourceCurrency) {
      this.sourceCurrency = sourceCurrency;
      return this;
    }

    public Builder targetCurrency(String targetCurrency) {
      this.targetCurrency = targetCurrency;
      return this;
    }

    public Builder sourceAmount(Long sourceAmount) {
      this.sourceAmount = sourceAmount;
      return this;
    }

    public Builder targetAmount(Long targetAmount) {
      this.targetAmount = targetAmount;
      return this;
    }

    public Builder rate(Double rate) {
      this.rate = rate;
      return this;
    }

    public Builder fee(Long fee) {
      this.fee = fee;
      return this;
    }

    public Builder message(String message) {
      this.message = message;
      return this;
    }

    public Builder quoteId(String quoteId) {
      this.quoteId = quoteId;
      return this;
    }

    public Builder status(PaymentStatus status) {
      this.status = status;
      return this;
    }
    
    public Builder expectedDeliveryDate(Date expectedDeliveryDate) {
      this.expectedDeliveryDate = expectedDeliveryDate;
      return this;
    }    

    public PaymentResult build() throws InvalidPropertyException {

      PaymentResult obj = new PaymentResult(this);
      validate(obj);
      return obj;
    }
  }

  public Optional<Long> getId() {
    return Optional.ofNullable(id);
  }

  public Long getUserId() {
    return userId;
  }

  public Optional<String> getSourceCurrency() {
    return Optional.ofNullable(sourceCurrency);
  }

  public Optional<String> getTargetCurrency() {
    return Optional.ofNullable(targetCurrency);
  }

  public Optional<Long> getSourceAmount() {
    return Optional.ofNullable(sourceAmount);
  }

  public Optional<Long> getTargetAmount() {
    return Optional.ofNullable(targetAmount);
  }

  public Optional<Double> getRate() {
    return Optional.ofNullable(rate);
  }

  public Optional<Long> getFee() {
    return Optional.ofNullable(fee);
  }

  public String getMessage() {
    return message;
  }

  public PaymentStatus getStatus() {
    return status;
  }

  public Optional<String> getQuoteId() {
    return Optional.ofNullable(quoteId);
  }
  
  public Optional<Date> getExpectedDeliveryDate() {
    return Optional.ofNullable(expectedDeliveryDate);
  }  

  @Override
  public String toString() {
    return "PaymentResult{" + "id=" + id + ", userId=" + userId + ", sourceCurrency=" + sourceCurrency + ", targetCurrency=" + targetCurrency + ", sourceAmount=" + sourceAmount + ", targetAmount=" + targetAmount + ", quoteId=" + quoteId + ", rate=" + rate + ", fee=" + fee + ", message=" + message + ", status=" + status + ", expectedDeliveryDate=" + expectedDeliveryDate + '}';
  }

  
  private static void validate(PaymentResult obj) throws InvalidPropertyException {
    if (null == obj) {
      throw new InvalidPropertyException("Null object");
    } else if (null == obj.getUserId() || obj.getUserId() < 1) {
      throw new InvalidPropertyException("Invalid Object. User Id not found.");
    }else if ( null == obj.getStatus()) {
      throw new InvalidPropertyException("Invalid Object. Payment Status must not be null.");
    }else if ( AppUtil.isEmptyString(obj.getMessage())) {
      throw new InvalidPropertyException("Invalid Object. Payment Message must not be null.");
    }
  }
}
