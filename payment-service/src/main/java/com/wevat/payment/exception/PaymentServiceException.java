package com.wevat.payment.exception;

public class PaymentServiceException extends Exception {

  private int code;

  /**
   *
   */
  public PaymentServiceException() {
    super();
  }

  /**
   * @param message
   */
  public PaymentServiceException(String message) {
    super(message);
  }

  public PaymentServiceException(int code, String locale) {
    this.code = code;
  }

  public PaymentServiceException(int code, String[] params, String locale) {
    this.code = code;
  }

  public PaymentServiceException(int code, String locale, Throwable cause) {
    this.code = code;
  }

  /**
   * @param message
   * @param cause
   */
  public PaymentServiceException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * @param cause
   */
  public PaymentServiceException(Throwable cause) {
    super(cause);
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

}
