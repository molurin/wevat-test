package com.wevat.payment.domain;

import com.wevat.core.exception.InvalidPropertyException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class PaymentTest {

  private static final Long sourceAmount = 100L;
  private static final String sourceCurrency = "GBP";
  private static final String destCurrency = "USD";
  private static final Long userId = 1L;

  @Test
  public void testValidPayment() {

    try {
      Payment payment = new Payment.Builder()
          .sourceAmount(sourceAmount)
          .sourceCurrency(sourceCurrency)
          .targetCurrency(destCurrency)
          .userId(userId)
          .build();
      assertNotNull(payment);
    } catch (InvalidPropertyException ex) {
      Logger.getLogger(PaymentTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Test(expected = InvalidPropertyException.class)
  public void testMissingUserIdPayment() throws InvalidPropertyException {
    try {
      new Payment.Builder()
          .sourceAmount(sourceAmount)
          .sourceCurrency(sourceCurrency)
          .targetCurrency(destCurrency)
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Object. User Id not found.", ex.getMessage());
      throw ex;
    }
  }
  
  @Test(expected = InvalidPropertyException.class)
  public void testMissingSourceCurrenyPayment() throws InvalidPropertyException {
    try {
      new Payment.Builder()
          .sourceAmount(sourceAmount)
          .userId(userId)
          .targetCurrency(destCurrency)
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Object. Source Currency must not be empty.", ex.getMessage());
      throw ex;
    }
  } 
  
  @Test(expected = InvalidPropertyException.class)
  public void testMissingTargetCurrenyPayment() throws InvalidPropertyException {
    try {
      new Payment.Builder()
          .sourceAmount(sourceAmount)
          .userId(userId)
          .sourceCurrency(sourceCurrency)
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Object. Target Currency must not be empty.", ex.getMessage());
      throw ex;
    }
  } 
  
  @Test(expected = InvalidPropertyException.class)
  public void testMissingAmountsPayment() throws InvalidPropertyException {
    try {
      new Payment.Builder()
          .userId(userId)
          .sourceCurrency(sourceCurrency)
          .targetCurrency(destCurrency)
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Object. Either Source Amount or Target Amount must be present.", ex.getMessage());
      throw ex;
    }
  }    

}
