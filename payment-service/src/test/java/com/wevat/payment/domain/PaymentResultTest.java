package com.wevat.payment.domain;

import com.wevat.core.exception.InvalidPropertyException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class PaymentResultTest {

  private static final Long sourceAmount = 100L;
  private static final Long destAmount = 170L;
  private static final String sourceCurrency = "GBP";
  private static final String destCurrency = "USD";
  private static final double rate = 1.7;
  private static final Long userId = 1L;
  private static final Long fee = 1L;
  private static final String quoteId = "1";

  @Test
  public void testValidPaymentResult() {

    try {
      PaymentResult payment = new PaymentResult.Builder()
          .id(1L)
          .fee(fee)
          .rate(rate)
          .sourceAmount(sourceAmount)
          .sourceCurrency(sourceCurrency)
          .targetAmount(destAmount)
          .targetCurrency(destCurrency)
          .userId(userId)
          .quoteId(quoteId)
          .expectedDeliveryDate(new Date())
          .status(PaymentStatus.COMPLETED)
          .message("Success")
          .build();
      assertNotNull(payment);
    } catch (InvalidPropertyException ex) {
      Logger.getLogger(PaymentTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Test(expected = InvalidPropertyException.class)
  public void testMissingUserIdInPaymentResult() throws InvalidPropertyException {
    try {
      new PaymentResult.Builder()
          .id(1L)
          .fee(fee)
          .rate(rate)
          .sourceAmount(sourceAmount)
          .sourceCurrency(sourceCurrency)
          .targetAmount(destAmount)
          .targetCurrency(destCurrency)
          .quoteId(quoteId)
          .expectedDeliveryDate(new Date())
          .status(PaymentStatus.COMPLETED)
          .message("Success")
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Object. User Id not found.", ex.getMessage());
      throw ex;
    }
  }

  @Test(expected = InvalidPropertyException.class)
  public void testMissingStatusInPaymentResult() throws InvalidPropertyException {
    try {
      new PaymentResult.Builder()
          .id(1L)
          .fee(fee)
          .rate(rate)
          .userId(userId)
          .sourceAmount(sourceAmount)
          .sourceCurrency(sourceCurrency)
          .targetAmount(destAmount)
          .targetCurrency(destCurrency)
          .quoteId(quoteId)
          .expectedDeliveryDate(new Date())
          .message("Success")
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Object. Payment Status must not be null.", ex.getMessage());
      throw ex;
    }
  }

  @Test(expected = InvalidPropertyException.class)
  public void testMissingMessageInPaymentResult() throws InvalidPropertyException {
    try {
      new PaymentResult.Builder()
          .id(1L)
          .fee(fee)
          .rate(rate)
          .sourceAmount(sourceAmount)
          .sourceCurrency(sourceCurrency)
          .targetAmount(destAmount)
          .targetCurrency(destCurrency)
          .userId(userId)
          .quoteId(quoteId)
          .expectedDeliveryDate(new Date())
          .status(PaymentStatus.COMPLETED)
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Object. Payment Message must not be null.", ex.getMessage());
      throw ex;
    }
  }

}
