package com.wevat.payment.service;

import com.wevat.core.exception.InvalidPropertyException;
import com.wevat.payment.data.paymentlog.Payments;
import com.wevat.payment.data.paymentlog.PaymentsRepository;
import com.wevat.payment.domain.Payment;
import com.wevat.payment.domain.PaymentResult;
import com.wevat.payment.domain.PaymentStatus;
import com.wevat.payment.exception.PaymentServiceException;
import com.wevat.payment.service.impl.PaymentServiceImpl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

public class PaymentServiceTest {

  private static final Long sourceAmount = 100L;
  private static final Long destAmount = 170L;
  private static final String sourceCurrency = "GBP";
  private static final String destCurrency = "USD";
  private static final double rate = 1.7;
  private static final Long userId = 1L;
  private static final Long fee = 1L;
  private static final String quoteId = "1";

  @InjectMocks
  PaymentService paymentService = new PaymentServiceImpl();
  @Mock
  PaymentsRepository paymentsRepository;
  @Mock
  PaymentProvider exchangeRateService;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void createPaymentTest() {
    try {
      Payment payment = buildPayment();
      when(paymentsRepository.save(any(Payments.class))).thenReturn(providePayments(PaymentStatus.COMPLETED));
      when(exchangeRateService.createExchangeRateQuote(any(Payment.class), any(Long.class))).thenReturn(providePaymentResult(PaymentStatus.COMPLETED));
      Optional<PaymentResult> result = paymentService.createPayment(payment);
      assertTrue(result.isPresent());
      assertEquals(result.get().getSourceAmount().get(), sourceAmount);
      assertEquals(result.get().getSourceCurrency().get(), sourceCurrency);
      assertEquals(result.get().getTargetAmount().get(), destAmount);
      assertEquals(result.get().getTargetCurrency().get(), destCurrency);

    } catch (InvalidPropertyException | PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  @Test
  public void createPaymentUnableToLogPaymentTest() {
    try {
      Payment payment = buildPayment();
      when(paymentsRepository.save(any(Payments.class))).thenReturn(null);
      when(exchangeRateService.createExchangeRateQuote(any(Payment.class), any(Long.class))).thenReturn(providePaymentResult(PaymentStatus.COMPLETED));
      Optional<PaymentResult> result = paymentService.createPayment(payment);
      assertFalse(result.isPresent());

    } catch (InvalidPropertyException | PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  @Test
  public void createPaymentExchangeRateFailedTest() {
    try {
      Payment payment = buildPayment();
      when(paymentsRepository.save(any(Payments.class))).thenReturn(providePayments(PaymentStatus.COMPLETED));
      when(exchangeRateService.createExchangeRateQuote(any(Payment.class), any(Long.class))).thenReturn(Optional.empty());
      Optional<PaymentResult> result = paymentService.createPayment(payment);
      assertFalse(result.isPresent());

    } catch (InvalidPropertyException | PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  @Test
  public void getUserPaymentsTest() {
    try {
      List<Payments> payments = new ArrayList<>();
      payments.add(providePayments(PaymentStatus.COMPLETED));
      when(paymentsRepository.findByUserId(userId)).thenReturn(payments);
      
      List<PaymentResult> result = paymentService.getUserPayments(userId);
      assertNotNull(result);
      assertEquals(result.size(), 1);
      assertEquals(result.get(0).getSourceCurrency().get(), sourceCurrency);
      assertEquals(result.get(0).getTargetAmount().get(), destAmount);
      assertEquals(result.get(0).getTargetCurrency().get(), destCurrency);

    } catch (PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
    @Test
  public void getUserPaymentsEmptyTest() {
    try {
      List<Payments> payments = new ArrayList<>();
      when(paymentsRepository.findByUserId(userId)).thenReturn(payments);
      
      List<PaymentResult> result = paymentService.getUserPayments(userId);
      assertNotNull(result);
      assertEquals(result.size(), 0);

    } catch (PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  @Test
  public void getPaymentTest() {
    try {
      Payments payment = providePayments(PaymentStatus.COMPLETED);
      when(paymentsRepository.getOne(1L)).thenReturn(payment);
      
      Optional<PaymentResult> result = paymentService.getPayment(1L);
      assertTrue(result.isPresent());
      assertEquals(result.get().getSourceAmount().get(), sourceAmount);
      assertEquals(result.get().getSourceCurrency().get(), sourceCurrency);
      assertEquals(result.get().getTargetAmount().get(), destAmount);
      assertEquals(result.get().getTargetCurrency().get(), destCurrency);

    } catch (PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  @Test
  public void getPaymentNotFoundTest() {
    try {
      when(paymentsRepository.getOne(1L)).thenReturn(null);
      
      Optional<PaymentResult> result = paymentService.getPayment(1L);
      assertNotNull(result);
      assertFalse(result.isPresent());

    } catch (PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }  

  private Payment buildPayment() throws InvalidPropertyException {
    return new Payment.Builder()
        .sourceAmount(sourceAmount)
        .sourceCurrency(sourceCurrency)
        .targetCurrency(destCurrency)
        .userId(userId)
        .build();
  }

  private Payments providePayments(PaymentStatus status) {
    Payments newPayment = new Payments();

    newPayment.setId(1L);
    newPayment.setDestinationAmount(destAmount);
    newPayment.setDestinationCurrency(destCurrency);
    newPayment.setPaymentStatus(status.status());
    newPayment.setPayoutCurrency(sourceCurrency);
    newPayment.setPayoutAmount(sourceAmount);
    newPayment.setUserId(userId);
    newPayment.setQuoteId(quoteId);
    newPayment.setRate(rate);
    newPayment.setFee(fee);
    newPayment.setDeliveryEstimate(new Date());
    newPayment.setHostResponseMessage(status.status());

    return newPayment;
  }

  private Optional<PaymentResult> providePaymentResult(PaymentStatus status) throws InvalidPropertyException {
    return Optional.ofNullable(new PaymentResult.Builder()
        .id(1L)
        .fee(fee)
        .rate(rate)
        .sourceAmount(sourceAmount)
        .sourceCurrency(sourceCurrency)
        .targetAmount(destAmount)
        .targetCurrency(destCurrency)
        .userId(userId)
        .quoteId(quoteId)
        .expectedDeliveryDate(new Date())
        .status(status)
        .message(status.status())
        .build());
  }
}
