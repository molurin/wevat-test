package com.wevat.payment.service;

import com.wevat.core.client.rest.RestClient;
import com.wevat.core.exception.InvalidPropertyException;
import com.wevat.core.exception.WevatException;
import com.wevat.core.util.AppUtil;
import com.wevat.payment.domain.Payment;
import com.wevat.payment.domain.PaymentResult;
import com.wevat.payment.exception.PaymentServiceException;
import com.wevat.payment.exchange.GetQuoteRequest;
import com.wevat.payment.exchange.GetQuoteResponse;
import com.wevat.payment.service.impl.transferwise.TransferwisePaymentProviderImpl;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class PaymentProviderTest {

  private static final Long sourceAmount = 100L;
  private static final Long destAmount = 170L;
  private static final String sourceCurrency = "GBP";
  private static final String destCurrency = "USD";
  private static final double rate = 1.7;
  private static final Long userId = 1L;
  private static final Long fee = 1L;
  private static final String quoteId = "1";

  @InjectMocks
  PaymentProvider paymentProvider = new TransferwisePaymentProviderImpl();
  @Mock
  RestTemplate restTemplate;
  @Mock
  RestClient client;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void createExchangeRateQuoteTest() {
    try {
      Payment payment = buildPayment();
      when(client.sendPOST(anyString(), any(GetQuoteRequest.class), any(HttpHeaders.class), any(Class.class))).thenReturn(new ResponseEntity<>(provideGetQuoteResponse(), HttpStatus.OK));
      Optional<PaymentResult> result = paymentProvider.createExchangeRateQuote(payment, userId);
      assertTrue(result.isPresent());
      assertEquals(result.get().getSourceAmount().get(), sourceAmount);
      assertEquals(result.get().getSourceCurrency().get(), sourceCurrency);
      assertEquals(result.get().getTargetAmount().get(), destAmount);
      assertEquals(result.get().getTargetCurrency().get(), destCurrency);

    } catch (InvalidPropertyException | PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (WevatException ex) {
      Logger.getLogger(PaymentProviderTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Test
  public void createExchangeRateQuoteFailedNoBodyTest() {
    try {
      Payment payment = buildPayment();
      when(client.sendPOST(anyString(), any(GetQuoteRequest.class), any(HttpHeaders.class), any(Class.class))).thenReturn(new ResponseEntity<>(null, HttpStatus.OK));
      Optional<PaymentResult> result = paymentProvider.createExchangeRateQuote(payment, userId);
      assertFalse(result.isPresent());

    } catch (InvalidPropertyException | PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (WevatException ex) {
      Logger.getLogger(PaymentProviderTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Test
  public void createExchangeRateQuoteFailedWithBodyTest() {
    try {
      Payment payment = buildPayment();
      when(client.sendPOST(anyString(), any(GetQuoteRequest.class), any(HttpHeaders.class), any(Class.class))).thenReturn(new ResponseEntity<>(provideGetQuoteResponse(), HttpStatus.BAD_REQUEST));
      Optional<PaymentResult> result = paymentProvider.createExchangeRateQuote(payment, userId);
      assertTrue(result.isPresent());
      assertEquals(result.get().getSourceAmount().get(), sourceAmount);
      assertEquals(result.get().getSourceCurrency().get(), sourceCurrency);
      assertEquals(result.get().getTargetAmount().get(), destAmount);
      assertEquals(result.get().getTargetCurrency().get(), destCurrency);

    } catch (InvalidPropertyException | PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (WevatException ex) {
      Logger.getLogger(PaymentProviderTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Test
  public void createExchangeRateQuoteNoResponseTest() {
    try {
      Payment payment = buildPayment();
      when(client.sendPOST(anyString(), any(GetQuoteRequest.class), any(HttpHeaders.class), any(Class.class))).thenReturn(null);
      Optional<PaymentResult> result = paymentProvider.createExchangeRateQuote(payment, userId);
      assertFalse(result.isPresent());

    } catch (InvalidPropertyException | PaymentServiceException ex) {
      Logger.getLogger(PaymentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
    } catch (WevatException ex) {
      Logger.getLogger(PaymentProviderTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private Payment buildPayment() throws InvalidPropertyException {
    return new Payment.Builder()
        .sourceAmount(sourceAmount)
        .sourceCurrency(sourceCurrency)
        .targetCurrency(destCurrency)
        .userId(userId)
        .build();
  }

  private GetQuoteResponse provideGetQuoteResponse() {
    GetQuoteResponse response = new GetQuoteResponse();
    response.setSource(sourceCurrency);
    response.setSourceAmount(AppUtil.toDecimal(sourceAmount));
    response.setTarget(destCurrency);
    response.setTargetAmount(AppUtil.toDecimal(destAmount));
    response.setRate(rate);
    response.setRateType("FIXED");
    return response;
  }
}
