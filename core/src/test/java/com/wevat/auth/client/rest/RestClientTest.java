package com.wevat.auth.client.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wevat.core.client.rest.RestClient;
import com.wevat.core.exception.WevatException;
import com.wevat.core.exchange.BaseResponse;
import com.wevat.core.util.AppUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class RestClientTest {

  @InjectMocks
  RestClient client;
  @Mock
  RestTemplate restTemplate;

  @Before
  public void setUp() {
    client = new RestClient();
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void restclientTest() {
    try {

      BaseResponse myobjectA = new BaseResponse();
      myobjectA.setCode("200");
      ResponseEntity<BaseResponse> responseEntity = new ResponseEntity<>(myobjectA, HttpStatus.OK);
      when(restTemplate.exchange(
          Matchers.anyString(),
          Matchers.any(HttpMethod.class),
          Matchers.<HttpEntity<?>>any(),
          Matchers.<Class<BaseResponse>>any())).thenReturn(responseEntity);

      ResponseEntity<BaseResponse> result = client.sendPOST("/objects", myobjectA, AppUtil.getCommonHttpHeader(), BaseResponse.class);
      assertNotNull(result);
      assertNotNull(result.getBody());
      assertEquals(myobjectA.getCode(), result.getBody().getCode());

    } catch (RestClientException | WevatException ex) {
      Logger.getLogger(RestClientTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
    @Test
  public void restclientHttpClientErrorExceptionTest() {
    try {
      
      BaseResponse myobjectA = new BaseResponse();
      myobjectA.setCode("200");
      
              ObjectMapper mapper = new ObjectMapper();
              String errorString = mapper.writeValueAsString(myobjectA);

        
      when(restTemplate.exchange(
          Matchers.anyString(),
          Matchers.any(HttpMethod.class),
          Matchers.<HttpEntity<?>>any(),
          Matchers.<Class<BaseResponse>>any())).thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST, errorString));

      ResponseEntity<BaseResponse> result = client.sendPOST("/objects", myobjectA, AppUtil.getCommonHttpHeader(),BaseResponse.class);
      assertNull(result);

    } catch (RestClientException | WevatException | JsonProcessingException ex) {
      Logger.getLogger(RestClientTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

}
