package com.wevat.auth.domain;

import com.wevat.core.domain.UserObject;
import com.wevat.core.exception.InvalidPropertyException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class UserObjectTest {


  @Test
  public void testValidUserObject() {

    try {
      UserObject user = new UserObject.Builder()
          .id(1L)
          .firstName("Alan")
          .lastName("Geek")
          .username("geekLord")
          .payoutCurrency("GBP")
          .build();
      assertNotNull(user);
    } catch (InvalidPropertyException ex) {
      Logger.getLogger(UserObjectTest.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Test(expected = InvalidPropertyException.class)
  public void testMissingFirstNameUserObject() throws InvalidPropertyException {
    try {
      new UserObject.Builder()
          .id(1L)
          .lastName("Geek")
          .username("geekLord")
          .payoutCurrency("GBP")
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Request. First Name is missing", ex.getMessage());
      throw ex;
    }
  }

  @Test(expected = InvalidPropertyException.class)
  public void testMissingLastnameInUserObject() throws InvalidPropertyException {
    try {
      new UserObject.Builder()
          .id(1L)
          .firstName("Alan")
          .username("geekLord")
          .payoutCurrency("GBP")
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Request. Last Name is missing", ex.getMessage());
      throw ex;
    }
  }

  @Test(expected = InvalidPropertyException.class)
  public void testMissingUsernameInUserObject() throws InvalidPropertyException {
    try {
      new UserObject.Builder()
          .id(1L)
          .firstName("Alan")
          .lastName("Geek")
          .payoutCurrency("GBP")
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Request. Username is missing", ex.getMessage());
      throw ex;
    }
  }

  @Test(expected = InvalidPropertyException.class)
  public void testMissingpayoutCurrencyInUserObject() throws InvalidPropertyException {
    try {
      new UserObject.Builder()
          .id(1L)
          .firstName("Alan")
          .lastName("Geek")
          .username("geekLord")
          .build();
    } catch (InvalidPropertyException ex) {
      assertEquals("Invalid Request. payoutCurrency is missing", ex.getMessage());
      throw ex;
    }
  }

}
