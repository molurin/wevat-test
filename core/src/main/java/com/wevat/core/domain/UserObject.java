package com.wevat.core.domain;

import com.wevat.core.exception.InvalidPropertyException;
import com.wevat.core.util.AppUtil;
import java.util.Optional;

public class UserObject {

  private final long    id;
  private final String  firstName;
  private final String  lastName;
  private final String  payoutCurrency;
  private final String  username;
  private final String  password;

  private UserObject(Builder builder) {
    this.id = builder.id;
    this.firstName = builder.firstName;
    this.lastName = builder.lastName;
    this.payoutCurrency = builder.payoutCurrency;
    this.username = builder.username;
    this.password = builder.password;
  }

  public static class Builder {

    private long id;
    private String firstName;
    private String lastName;
    private String password;
    private String payoutCurrency;
    private String username;

    public Builder id(long id) {
      this.id = id;
      return this;
    }

    public Builder firstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public Builder lastName(String lastName) {
      this.lastName = lastName;
      return this;
    }

    public Builder payoutCurrency(String payoutCurrency) {
      this.payoutCurrency = payoutCurrency;
      return this;
    }
    
    public Builder username(String username) {
      this.username = username;
      return this;
    }
    
    public Builder password(String password) {
      this.password = password;
      return this;
    }    

    public UserObject build() throws InvalidPropertyException {

      UserObject userObject = new UserObject(this);
      validate(userObject);
      return userObject;
    }
  }

  public long getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getPayoutCurrency() {
    return payoutCurrency;
  }
  
  public String getUsername() {
    return username;
  }  

  public Optional<String> getPassword() {
    return Optional.ofNullable(password);
  }

  @Override
  public String toString() {
    return "UserObject{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", payoutCurrency=" + payoutCurrency + ", username=" + username + ", password=" + password + '}';
  }
  


  private static void validate(UserObject userObject) throws InvalidPropertyException {
    if ( null == userObject ) {
      throw new InvalidPropertyException("Null object");
    } else if ( userObject.getId() < 1 ) {
      throw new InvalidPropertyException("Invalid Request. Id is invalid.");
    }else if ( AppUtil.isEmptyString(userObject.getUsername()) ) {
      throw new InvalidPropertyException("Invalid Request. Username is missing");
    } else if ( AppUtil.isEmptyString(userObject.getFirstName()) ) {
      throw new InvalidPropertyException("Invalid Request. First Name is missing");
    } else if ( AppUtil.isEmptyString(userObject.getLastName()) ) {
      throw new InvalidPropertyException("Invalid Request. Last Name is missing");
    } else if ( AppUtil.isEmptyString(userObject.getPayoutCurrency()) ) {
      throw new InvalidPropertyException("Invalid Request. payoutCurrency is missing");
    }
  }
}
