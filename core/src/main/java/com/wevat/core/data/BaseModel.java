package com.wevat.core.data;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

public class BaseModel implements Serializable {

  @Column
  @CreationTimestamp
  private LocalDateTime createOn;

  @Column
  @UpdateTimestamp
  private LocalDateTime lastUpdatedOn;

  public LocalDateTime getCreateOn() {
    return createOn;
  }

  public void setCreateOn(LocalDateTime createOn) {
    this.createOn = createOn;
  }

  public LocalDateTime getLastUpdatedOn() {
    return lastUpdatedOn;
  }

  public void setLastUpdatedOn(LocalDateTime lastUpdatedOn) {
    this.lastUpdatedOn = lastUpdatedOn;
  }

}
