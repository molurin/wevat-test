package com.wevat.core.exchange;

public class Error {

  //@ApiModelProperty(value = "", required = true)
  private String fieldName;
  //@ApiModelProperty(value = "", required = true)
  private String message;
  private String code;

  public Error() {
  }

  public Error(String fieldName, String message) {
    this.fieldName = fieldName;
    this.message = message;
  }

  public String getFieldName() {
    return fieldName;
  }

  public String getMessage() {
    return message;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public String toString() {
    return "Error{" + "fieldName=" + fieldName + ", message=" + message + ", code=" + code + '}';
  }

}
