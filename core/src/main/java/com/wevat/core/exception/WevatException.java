package com.wevat.core.exception;

public class WevatException extends Exception {

  private int code;

  /**
   *
   */
  public WevatException() {
    super();
  }

  /**
   * @param message
   */
  public WevatException(String message) {
    super(message);
  }

  public WevatException(int code, String locale) {
    this.code = code;
  }

  public WevatException(int code, String[] params, String locale) {
    this.code = code;
  }

  public WevatException(int code, String locale, Throwable cause) {
    this.code = code;
  }

  /**
   * @param message
   * @param cause
   */
  public WevatException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * @param cause
   */
  public WevatException(Throwable cause) {
    super(cause);
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

}
