package com.wevat.core.exception;

public class ACustomException extends Exception {

  private int code;

  /**
   *
   */
  public ACustomException() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   */
  public ACustomException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public ACustomException(int code, String locale) {
    //super(Locale.getInstance(locale).getProperty(code));
    this.code = code;
    // TODO Auto-generated constructor stub
  }

  public ACustomException(int code, String[] params, String locale) {
        //super(Locale.getInstance(locale).getProperty(code, params));
    // TODO Auto-generated constructor stub
    this.code = code;
  }

  public ACustomException(int code, String locale, Throwable cause) {
    	//super(Locale.getInstance(locale).getProperty(code), cause);
    // TODO Auto-generated constructor stub
    this.code = code;
  }

  /**
   * @param message
   * @param cause
   */
  public ACustomException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param cause
   */
  public ACustomException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

}
