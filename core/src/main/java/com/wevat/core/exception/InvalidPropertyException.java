package com.wevat.core.exception;

public class InvalidPropertyException extends ACustomException {

  /**
   *
   */
  public InvalidPropertyException() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   */
  public InvalidPropertyException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   * @param cause
   */
  public InvalidPropertyException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param cause
   */
  public InvalidPropertyException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

}
