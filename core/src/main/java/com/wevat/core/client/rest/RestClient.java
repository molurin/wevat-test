package com.wevat.core.client.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wevat.core.exception.WevatException;
import com.wevat.core.util.AppUtil;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Component
@Scope(value="request", proxyMode= ScopedProxyMode.TARGET_CLASS)
public class RestClient<T, S> {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  
  @Autowired
  RestTemplate restTemplate;


  public ResponseEntity<T> sendPOST(String serviceUrl, T requestBody, HttpHeaders headers, Class<T> returnedResponseClass) throws WevatException {
    HttpEntity request = new HttpEntity(requestBody, headers);

    try {
      logRequest(requestBody);
      Long requestTime = new Date().getTime();
      ResponseEntity<T> responseEntity = this.restTemplate.exchange(serviceUrl, HttpMethod.POST, request, returnedResponseClass);
      Long responseTime = new Date().getTime();
      long diff = responseTime - requestTime;
      long diffMilli = TimeUnit.MILLISECONDS.toMillis(diff);
      StringBuilder builder = new StringBuilder();
      builder.append("HostResponseTimeMetrics:");
      builder.append("Time=");
      builder.append(diffMilli);
      builder.append(";");
      builder.append("Endpoint=");
      builder.append(serviceUrl);

      logger.info(builder.toString());
      logResponse(responseEntity);
      return responseEntity;
    } catch (ResourceAccessException e) {
      logger.debug("ResourceAccessException - " + e.getMessage());
      throw new WevatException(e.getMessage());
    } catch (HttpClientErrorException e) {
      HttpStatus status = e.getStatusCode();
      if (HttpStatus.UNAUTHORIZED.value() == status.value()) {
        return new ResponseEntity<>(status);
      }
      String errorString = e.getResponseBodyAsString();
      if (null != errorString) {
        logger.debug("Host Error - " + errorString);
        ObjectMapper mapper = new ObjectMapper();
        try {
          return new ResponseEntity<>(mapper.readValue(errorString, returnedResponseClass), status);
        } catch (IOException ex) {
          logger.debug("Unable to cast error - " + e.getMessage());
        }
      }

    } catch (HttpServerErrorException e) {
      HttpStatus status = e.getStatusCode();
      String errorString = e.getResponseBodyAsString();
      if (!AppUtil.isEmptyString(errorString)) {
        logger.debug("Host Error - " + errorString);
        ObjectMapper mapper = new ObjectMapper();
        try {
          return new ResponseEntity<>(mapper.readValue(errorString, returnedResponseClass), status);
        } catch (IOException ex) {
          logger.debug("Unable to cast error - " + e.getMessage());
        }
      }

    } catch (Exception e) {
      logger.debug("RestClientException - " + e.getMessage());
      throw new WevatException(e.getMessage());
    }
    return null;
  }

  private void logRequest(T requestBody) {
    if (null != requestBody) {
      logger.debug(requestBody.toString());
    }
  }

  private void logResponse(ResponseEntity<T> responseEntity) {

    if (null != responseEntity && null != responseEntity.getBody()) {
      logger.debug(responseEntity.getBody().toString());
    }
  }

}
