package com.wevat.core;


import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;


@Configuration
public class RestAPIWebConfig {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Bean
    public ClientHttpRequestFactory clientHttpRequestFactory() {
        return new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
    }

    @Bean
    public RestTemplate restTemplate(ClientHttpRequestFactory clientHttpRequestFactory) {
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                logger.debug("Response Status Code: " + response.getRawStatusCode());
                logger.debug("Response Status Text: " + response.getStatusText());
                if (response.getRawStatusCode() == 404 || response.getRawStatusCode() == 502 || response.getRawStatusCode() == 503 || response.getRawStatusCode() == 504) {//usually there is no response to parse in this case
                    logger.debug("Response Body: " + new String(getResponseBody(response)));
                    return true;
                }
                return false;
            }
        });

        return restTemplate;
    }
}
