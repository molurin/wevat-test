package com.wevat.core.util;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class AppUtil {

  private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  public static final String USER_IN_REQUEST = "USER_IN_REQUEST";

  public static boolean isEmptyString(String str) {
    return str == null || str.equals("");
  }

  public static boolean isPhoneNumber(String text) {
    if (text.matches("[0-9]+") && text.length() > 10) {
      return true;
    }

    return false;
  }

  public static boolean isValidEmail(String email) {
    Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
    Matcher mat = pattern.matcher(email);

    if (mat.matches()) {

      return true;
    } else {

      return false;
    }
  }

  public static String randomAlphaNumeric(int count) {
    StringBuilder builder = new StringBuilder();
    while (count-- != 0) {
      int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
      builder.append(ALPHA_NUMERIC_STRING.charAt(character));
    }
    return builder.toString();
  }

  public static HttpHeaders getCommonHttpHeader() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(new MediaType("application", "json"));
    return headers;
  }

  public static HttpHeaders getAuthorizationHeader(HttpHeaders headers, String token) {
    if (null == headers) {
      headers = new HttpHeaders();
    }
    headers.add("Authorization", "Bearer " + token);

    return headers;
  }
  
  public static Double toDecimal(Long amount) {
    if ( null == amount ) return 0D;
    BigDecimal x100 = new BigDecimal(100);
    BigDecimal val = BigDecimal.valueOf(amount).setScale(2);
    return val.divide(x100).setScale(2).doubleValue();
  }

  public static Long fromDecimal(Double amount) {
    if ( null == amount ) return 0L;
    BigDecimal x100 = new BigDecimal(100);
    BigDecimal val = BigDecimal.valueOf(amount).setScale(2);
    return val.multiply(x100).longValueExact();
  }

}
